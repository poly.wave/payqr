var cluster = require('cluster');

if (cluster.isMaster) {
  // Count the machine's CPUs
  var cpuCount = require('os').cpus().length;

  console.log('Master cluster setting up ' + cpuCount + ' workers...');

  // Create a worker for each CPU
  for (var i = 0; i < cpuCount; i += 1) {
    cluster.fork();
  }

  // Listen for online workers
  cluster.on('online', function(worker) {
    console.log('Worker ' + worker.process.pid + ' is online!');
  });

  // Listen for offline workers
  cluster.on('exit', function(worker, code, signal) {
    console.log('Worker ' + worker.process.pid + ' is offline!');
    cluster.fork();
  });

} else {
  require('.');
}
