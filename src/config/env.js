const Joi = require('joi');

// require and configure dotenv, will load vars in .env in PROCESS.ENV
require('dotenv').config();

// define validation for all the env vars
const envVarsSchema = Joi.object({
  DOMAIN: Joi.string()
    .default('skyqr.com'),

  NODE_ENV: Joi.string()
    .allow([
      'development', 'production', 'test', 'provision'
    ])
    .default('development'),

  PORT: Joi.number()
    .default(3000),

  JWT_SECRET: Joi.string().required()
    .description('JWT Secret required to sign'),

  DATABASE_DBNAME: Joi.string().required()
    .description('Mongo DB host url'),
  DATABASE_USERNAME: Joi.string().required()
    .description('Mongo DB host url'),
  DATABASE_PASSWORD: Joi.string().required()
    .description('Mongo DB host url'),
  DATABASE_HOST: Joi.string().required()
    .description('Mongo DB host url'),
  DATABASE_DIALECT: Joi.string().required()
    .description('Mongo DB host url'),

  TWILIO_TOKEN: Joi.string().required()
    .default('0797976eff78ce370854d12fa4526670'),
  TWILIO_SID: Joi.string().required()
    .default('AC952140c0e653ae9b86c86a8db393814d'),
  TWILIO_NUMBER: Joi.number()
    .default(18887871258),

  REDIS_HOST: Joi.string().required(),
  REDIS_PORT: Joi.string().required(),
  REDIS_PASS: Joi.string().required(),

  STRIPE_SECRET: Joi.string().required()
}).unknown()
  .required();



const {
  error, value: envVars
} = Joi.validate(process.env, envVarsSchema);

if (error) {
  throw new Error(`Config validation error: ${error.message}`);
}

const config = {
  domain: envVars.DOMAIN,
  env: envVars.NODE_ENV,
  port: envVars.PORT,
  jwtSecret: envVars.JWT_SECRET,
  twilio: {
    token: envVars.TWILIO_TOKEN,
    sid: envVars.TWILIO_SID,
    number: envVars.TWILIO_NUMBER,
  },
  database: {
    dbname: envVars.DATABASE_DBNAME,
    username: envVars.DATABASE_USERNAME,
    password: envVars.DATABASE_PASSWORD,
    host: envVars.DATABASE_HOST,
    dialect: envVars.DATABASE_DIALECT,
  },
  redis: {
    host: envVars.REDIS_HOST,
    port: envVars.REDIS_PORT,
    password: envVars.REDIS_PASS
  },
  stripe: {
    secret_key: envVars.STRIPE_SECRET
  }
};

module.exports = config;
