const util = require('util');
// config should be imported before importing any other file
const config = require('./config/env');
const app = require('./app/app');

// make bluebird default Promise
Promise = require('bluebird');

module.exports = app.listen(config.port, () => {
  console.info(`server started on port ${config.port} (${config.env})`);
});
