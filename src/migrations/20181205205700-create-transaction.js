'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Transactions', {
      id: {
        allowNull: false,
        primaryKey: true,
        unique: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.literal('uuid_generate_v4()')
      },
      description: {
        allowNull: true,
        type: Sequelize.TEXT
      },
      type: {
        allowNull: false,
        type: Sequelize.ENUM,
        values: ['static', 'dynamic']
      },
      status: {
        type: Sequelize.ENUM,
        values: ['new', 'pending', 'complete', 'rejected', 'canceled', 'refunded_p', 'refunded'],
        defaultValue: 'new'
      },
      amount: {
        type: Sequelize.DECIMAL(12,2)
      },
      fee: {
        type: Sequelize.DECIMAL(12,2)
      },
      currency: {
        type: Sequelize.STRING,
        defaultValue: 'usd'
      },
      paid: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      livemode: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      source_id: {
        allowNull: true,
        type: Sequelize.INTEGER
      },
      dispute_id: {
        allowNull: true,
        type: Sequelize.INTEGER
      },
      merchant_id: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      user_id: {
        allowNull: true,
        type: Sequelize.UUID
      },
      cashier_id: {
        allowNull: true,
        type: Sequelize.UUID
      }, //null if user create this transaction
      expires_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      removedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Transactions');
  }
};
