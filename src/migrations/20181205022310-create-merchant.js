'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Merchants', {
      id: {
        allowNull: false,
        primaryKey: true,
        unique: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.literal('uuid_generate_v4()')
      },
      legalName: {
        allowNull: true,
        type: Sequelize.STRING
      },
      dbaName: {
        allowNull: true,
        type: Sequelize.STRING
      },
      avatarUrl: {
        allowNull: true,
        type: Sequelize.STRING
      },
      address_line1: {
        allowNull: true,
        type: Sequelize.STRING
      },
      address_line2: {
        allowNull: true,
        type: Sequelize.STRING
      },
      address_state: {
        allowNull: true,
        type: Sequelize.STRING
      },
      address_zip: {
        allowNull: true,
        type: Sequelize.STRING
      },
      address_city: {
        allowNull: true,
        type: Sequelize.STRING
      },
      address_country: {
        allowNull: true,
        type: Sequelize.STRING
      },
      timezone: {
        type: Sequelize.STRING,
        defaultValue: 'America/Los_Angeles'
      },
      phoneNumber: {
        allowNull: true,
        type: Sequelize.STRING
      },
      faxNumber: {
        allowNull: true,
        type: Sequelize.STRING
      },
      email: {
        allowNull: true,
        type: Sequelize.STRING
      },
      ein: {
        allowNull: true,
        type: Sequelize.STRING
      },
      businessType: {
        type: Sequelize.STRING,
        defaultValue: 'corporation'
      },
      ipnUrl: {
        allowNull: true,
        type: Sequelize.STRING
      },
      apiSecret: {
        type: Sequelize.STRING
      },
      apiHash: {
        type: Sequelize.STRING
      },
      status: {
        type: Sequelize.ENUM,
        values: ['active', 'inactive', 'deleted'],
        defaultValue: 'inactive'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      removedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Merchants');
  }
};
