'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('UserCards', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      token: {
        allowNull: false,
        type: Sequelize.STRING
      },
      fingerprint: {
        allowNull: false,
        type: Sequelize.STRING
      },
      last4: {
        allowNull: true,
        type: Sequelize.INTEGER
      },
      brand: {
        allowNull: true,
        type: Sequelize.STRING
      },
      exp_month: {
        allowNull: true,
        type: Sequelize.INTEGER
      },
      exp_year: {
        allowNull: true,
        type: Sequelize.INTEGER
      },
      default: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      user_id: {
        allowNull: false,
        type: Sequelize.UUID,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      removedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('UserCards');
  }
};
