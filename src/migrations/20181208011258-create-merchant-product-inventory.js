'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('MerchantProductInventories', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      imageUrl: {
        allowNull: true,
        type: Sequelize.STRING
      },
      currency: {
        type: Sequelize.STRING,
        defaultValue: 'usd'
      },
      amount: {
        allowNull: true,
        type: Sequelize.DECIMAL(12,2)
      },
      tags: {
        allowNull: true,
        type: Sequelize.TEXT
      },
      inventory: {
        type: Sequelize.ENUM,
        values: ['finite', 'infinite'],
        defaultValue: 'infinite'
      },
      availability: {
        type: Sequelize.INTEGER,
        defaultValue: 1
      },
      active: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      merchant_product_id: {
        allowNull: false,
        type: Sequelize.UUID
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      removedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('MerchantProductInventories');
  }
};
