'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Users', {
      id: {
        allowNull: false,
        type: Sequelize.UUID,
        unique: true,
        defaultValue: Sequelize.literal('uuid_generate_v4()'),
        primaryKey: true
      },
      firstName: {
        type: Sequelize.STRING,
        allowNull: true
      },
      lastName: {
        type: Sequelize.STRING,
        allowNull: true
      },
      mobileNumber: {
        type: Sequelize.STRING,
        validate: {
          isNumeric:{
            msg: "Must be Phone Number"
          }
        },
        allowNull: false
      },
      email: {
        type: Sequelize.STRING,
        validate: {
          isEmail:{
            msg: "Must be E-Mail address"
          }
        },
        allowNull: true
      },
      ip: {
        type: Sequelize.STRING,
        validate: {
          isIP:{
            msg: "Must be IP address"
          }
        },
        allowNull: true
      },
      role: {
        type: Sequelize.ENUM,
        values: ['user', 'manager', 'admin', 'banned'],
        defaultValue: 'user'
      },
      avatarUrl: {
        type: Sequelize.STRING,
        allowNull: true
      },
      active: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      stripe_id: {
        allowNull: true,
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      removedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Users');
  }
};
