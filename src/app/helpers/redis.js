var Redis = require('ioredis');
const config = require('../../config/env');

var client = new Redis({
  port: config.redis.port,          // Redis port
  host: config.redis.host,   // Redis host
  family: 4,           // 4 (IPv4) or 6 (IPv6)
  password: config.redis.password,
  db: 0
});

client.on('error', function (err) {
  console.log('Error ' + err);
});

module.exports = client;
