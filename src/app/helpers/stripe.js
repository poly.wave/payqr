const config = require('../../config/env');
const stripe = require('stripe')(config.stripe.secret_key);

const onRequest = (request) => {
  console.log(request)
};

stripe.on('request', onRequest);

stripe.setAppInfo({
  name: 'SkyQR',
  version: '0.0.1',
  url: 'https://' + config.domain,
});

module.exports = stripe;
