const Joi = require('joi');

let validator = {
  update: {
    body: {
      firstName: Joi.string().regex(/^[a-zA-Z]+$/),
      lastName: Joi.string().regex(/^[a-zA-Z]+$/),
      email: Joi.string().email()
    }
  },

  updateAvatar: {
    body: {
      avatar: Joi.any().meta({
        swaggerType: 'file'
      }).optional().allow('').description('image file')
    }
  }
};

module.exports = validator;
