const Joi = require('joi');

let validator = {
  create: {
    body: {
      token: Joi.string().required(),
      default: Joi.boolean().required()
    }
  },

  show: {
    body: {
      user_card_id: Joi.string().required()
    }
  },

  remove: {
    body: {
      user_card_id: Joi.string().required()
    }
  }
};

module.exports = validator;
