const Joi = require('joi');

let validator = {
  login: {
    body: {
      mobileNumber: Joi.string().required(),
      pin: Joi.string()
    }
  }
};

module.exports = validator;
