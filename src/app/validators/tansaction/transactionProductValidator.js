const Joi = require('joi');

let validator = {
  create: {
    body: {
      merchant_product_id: Joi.number().integer().required(),
      transaction_id: Joi.number().integer().required()
    }
  },

  show: {
    body: {
      transaction_product_id: Joi.number().integer().required(),
      merchant_product_id: Joi.number().integer().required(),
      transaction_id: Joi.number().integer().required()
    }
  },

  showall: {
    body: {
      merchant_product_id: Joi.number().integer().required(),
      transaction_id: Joi.number().integer().required()
    }
  },

  remove: {
    body: {
      transaction_product_id: Joi.string().required(),
      merchant_product_id: Joi.number().integer().required(),
      transaction_id: Joi.number().integer().required()
    }
  }
};

module.exports = validator;
