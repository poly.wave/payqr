const Joi = require('joi');

let validator = {
  create: {
    body: {
      merchant_id: Joi.string().required(),
      source_id: Joi.number().integer(),
      dispute_id: Joi.number().integer(),
      cashier_id: Joi.string(),
      description: Joi.string(),
      type: Joi.array(),
      status: Joi.array(),
      amount: Joi.number(),
      currency: Joi.string(),
      paid: Joi.boolean(),
      fee: Joi.number(),
      livemode: Joi.boolean(),
      expires_at: Joi.string(),
    }
  },

  update: {
    body: {
      transaction_id: Joi.string().required(),
      merchant_id: Joi.string().required(),
      source_id: Joi.number().integer(),
      dispute_id: Joi.number().integer(),
      cashier_id: Joi.string(),
      description: Joi.string(),
      type: Joi.array(),
      status: Joi.array(),
      amount: Joi.number(),
      currency: Joi.string(),
      paid: Joi.boolean(),
      fee: Joi.number(),
      livemode: Joi.boolean(),
      expires_at: Joi.string(),
    }
  },

  show: {
    body: {
      transaction_id: Joi.number().integer().required(),
      merchant_id: Joi.string().required()
    }
  },

  showall: {
    body: {
      merchant_id: Joi.string().required()
    }
  },

  remove: {
    body: {
      transaction_id: Joi.string().required(),
      merchant_id: Joi.string().required()
    }
  }
};

module.exports = validator;
