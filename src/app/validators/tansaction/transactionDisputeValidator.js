const Joi = require('joi');

let validator = {
  create: {
    body: {
      transaction_id: Joi.number().integer().required()
    }
  },

  show: {
    body: {
      transaction_dispute_id: Joi.number().integer().required(),
      transaction_id: Joi.number().integer().required()
    }
  },

  showall: {
    body: {
      transaction_id: Joi.number().integer().required()
    }
  },

  remove: {
    body: {
      transaction_dispute_id: Joi.string().required(),
      transaction_id: Joi.number().integer().required()
    }
  }
};

module.exports = validator;
