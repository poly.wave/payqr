const Joi = require('joi');

let validator = {
  create: {
    body: {
      legalName: Joi.string().required()
    }
  },

  update: {
    body: {
      merchant_id: Joi.string().required(),
      legalName: Joi.string(),
      dbaName: Joi.string(),
      address_line1: Joi.string(),
      address_line2: Joi.string(),
      address_state: Joi.string(),
      address_zip: Joi.string(),
      address_city: Joi.string(),
      address_country: Joi.string(),
      phoneNumber: Joi.string(),
      faxNumber: Joi.string(),
      email: Joi.string().email(),
      ein: Joi.string(),
      businessType: Joi.string(),
      ipnUrl: Joi.string(),
      avatar: Joi.any().meta({
        swaggerType: 'file'
      }).optional().allow('').description('image file')
    }
  },

  show: {
    body: {
      merchant_id: Joi.string().required()
    }
  },

  remove: {
    body: {
      merchant_id: Joi.string().required()
    }
  }
};

module.exports = validator;
