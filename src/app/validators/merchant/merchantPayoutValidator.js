const Joi = require('joi');

let validator = {
  create: {
    body: {
      stripe_id: Joi.string().required(),
      merchant_id: Joi.number().integer().required(),
      amount: Joi.number(),
      destination: Joi.string(),
      arrival_date: Joi.date(),
      created_date: Joi.date()
    }
  },

  update: {
    body: {
      merchant_payout_id: Joi.number().integer(),
      merchant_id: Joi.number().integer().required(),
      stripe_id: Joi.string(),
      amount: Joi.number(),
      destination: Joi.string(),
      arrival_date: Joi.date(),
      created_date: Joi.date()
    }
  },

  show: {
    body: {
      merchant_payout_id: Joi.string().required(),
      merchant_id: Joi.number().integer().required(),
    }
  },

  showall: {
    body: {
      merchant_id: Joi.string().required()
    }
  },

  remove: {
    body: {
      merchant_payout_id: Joi.string().required(),
      merchant_id: Joi.number().integer().required(),
    }
  }
};

module.exports = validator;
