const Joi = require('joi');

let validator = {
  create: {
    body: {
      merchant_id: Joi.string().required(),
      name: Joi.string(),
    }
  },

  update: {
    body: {
      merchant_product_category_id: Joi.number().integer().required(),
      merchant_id: Joi.string().required(),
      name: Joi.string(),
    }
  },

  show: {
    body: {
      merchant_product_category_id: Joi.string().required(),
      merchant_id: Joi.string().required()
    }
  },

  showall: {
    body: {
      merchant_id: Joi.string().required()
    }
  },

  remove: {
    body: {
      merchant_product_category_id: Joi.string().required(),
      merchant_id: Joi.string().required()
    }
  }
};


module.exports = validator;
