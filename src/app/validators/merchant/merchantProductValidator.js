const Joi = require('joi');

let validator = {
  create: {
    body: {
      merchant_id: Joi.string().required(),
      category_id: Joi.number().integer(),
      name: Joi.string(),
      caption: Joi.string(),
      description: Joi.string(),
      url: Joi.string(),
      tags: Joi.string(),
      active: Joi.boolean(),
      image: Joi.any().meta({
        swaggerType: 'file'
      }).optional().allow('').description('image file'),
    }
  },

  update: {
    body: {
      merchant_product_id: Joi.string().required(),
      merchant_id: Joi.string().required(),
      category_id: Joi.string(),
      name: Joi.string(),
      caption: Joi.string(),
      description: Joi.string(),
      url: Joi.string(),
      tags: Joi.string(),
      active: Joi.boolean(),
      image: Joi.any().meta({
        swaggerType: 'file'
      }).optional().allow('').description('image file'),
    }
  },

  show: {
    body: {
      merchant_product_id: Joi.string().required(),
      merchant_id: Joi.string().required()
    }
  },

  showall: {
    body: {
      merchant_id: Joi.string().required()
    }
  },

  remove: {
    body: {
      merchant_product_id: Joi.string().required(),
      merchant_id: Joi.string().required()
    }
  }
};

module.exports = validator;
