const Joi = require('joi');

let validator = {
  create: {
    body: {
      merchant_product_id: Joi.string().required(),
      key: Joi.string(),
      value: Joi.string(),
    }
  },

  update: {
    body: {
      merchant_product_metadata_id: Joi.number().integer().required(),
      merchant_product_id: Joi.string().required(),
      key: Joi.string(),
      value: Joi.string(),
    }
  },

  show: {
    body: {
      merchant_product_metadata_id: Joi.string().required(),
      merchant_product_id: Joi.string().required()
    }
  },

  showall: {
    body: {
      merchant_product_id: Joi.string().required()
    }
  },

  remove: {
    body: {
      merchant_product_metadata_id: Joi.string().required(),
      merchant_product_id: Joi.string().required()
    }
  }
};

module.exports = validator;
