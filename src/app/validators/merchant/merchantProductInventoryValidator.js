const Joi = require('joi');

let validator = {
  create: {
    body: {
      merchant_product_id: Joi.string().required(),
      currency: Joi.string(),
      amount: Joi.string(),
      inventory: Joi.array(),
      availability: Joi.number(),
      tags: Joi.string(),
      active: Joi.boolean(),
      image: Joi.any().meta({
        swaggerType: 'file'
      }).optional().allow('').description('image file'),
    }
  },

  update: {
    body: {
      merchant_product_inventory_id: Joi.number().integer().required(),
      merchant_product_id: Joi.string().required(),
      currency: Joi.string(),
      amount: Joi.number(),
      inventory: Joi.array(),
      availability: Joi.number(),
      tags: Joi.string(),
      active: Joi.boolean(),
      image: Joi.any().meta({
        swaggerType: 'file'
      }).optional().allow('').description('image file'),
    }
  },

  show: {
    body: {
      merchant_product_inventory_id: Joi.string().required(),
      merchant_product_id: Joi.string().required()
    }
  },

  showall: {
    body: {
      merchant_product_id: Joi.string().required()
    }
  },

  remove: {
    body: {
      merchant_product_inventory_id: Joi.string().required(),
      merchant_product_id: Joi.string().required()
    }
  }
};

module.exports = validator;
