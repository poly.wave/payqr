const config = require('../../config/env');
const express = require('express');
const router = express.Router();
const jwt = require('express-jwt');

// Routes
const {
  adminRoutes,
  authRoutes,
  merchantProductRoutes,
  merchantProductCategoryRoutes,
  merchantRoutes,
  userRoutes,
  userCardRoutes
} = require('./v1');

// Middlewares
const {
  currentUserMiddleware,
  currentMerchantMiddleware,
  currentTransactionMiddleware
} = require('../middlewares');

router.get(
  '/',
  (req, res) => res.status(200).send({
    error: false,
    message: 'SkyQR API',
  })
);

router.use(
  jwt({
    secret: config.jwtSecret
  })
  .unless({
    path: /\/api\/auth\/*/i
  })
);

router.use(currentUserMiddleware.currentUser());
router.use(currentMerchantMiddleware.currentMerchant());
router.use(currentTransactionMiddleware.currentTransaction());

router.use('/admin', adminRoutes);

router.use('/auth', authRoutes);
router.use('/user', userRoutes);
router.use('/user/card', userCardRoutes);

router.use('/merchant', merchantRoutes);
router.use('/merchant/product', merchantProductRoutes);
router.use('/merchant/product/category', merchantProductCategoryRoutes);


module.exports = router;
