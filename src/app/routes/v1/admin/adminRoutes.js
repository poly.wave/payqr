const express = require('express');
const router = express.Router();

const controller = require('../../../controllers/admin/adminController');

router.get(
  '/test',
  controller.test
);

module.exports = router;
