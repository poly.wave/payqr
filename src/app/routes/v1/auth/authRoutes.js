const express = require('express');
const router = express.Router();
const validate = require('express-validation');

const {
  authValidator
} = require('../../../validators');
const controller = require('../../../controllers/auth/authController');

router.post(
  '/login',
  validate(authValidator.login),
  controller.login
);

module.exports = router;
