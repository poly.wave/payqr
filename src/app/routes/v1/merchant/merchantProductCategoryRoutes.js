const express = require('express');
const router = express.Router();
const validate = require('express-validation');

const {
  merchantProductCategoryValidator
} = require('../../../validators');

const {
  checkPermission
} = require('../../../middlewares/merchant/currentMerchantMiddleware');

const controller = require('../../../controllers/merchant/merchantProductCategoryController');

//Validation
validate.options({
  allowUnknownBody: false,
  allowUnknownHeaders: false,
  allowUnknownQuery: false,
  allowUnknownParams: false,
  allowUnknownCookies: false
});

router.get(
  '/show',
  validate(merchantProductCategoryValidator.show),
  checkPermission(['admin', 'manager', 'user']),
  controller.show
);

router.get(
  '/showall',
  validate(merchantProductCategoryValidator.showall),
  checkPermission(['admin', 'manager', 'user']),
  controller.showall
);

router.get(
  '/remove',
  validate(merchantProductCategoryValidator.remove),
  checkPermission(['admin', 'manager']),
  controller.remove
);

router.post(
  '/create',
  validate(merchantProductCategoryValidator.create),
  checkPermission(['admin', 'manager']),
  controller.create
);

router.post(
  '/update',
  validate(merchantProductCategoryValidator.update),
  checkPermission(['admin', 'manager']),
  controller.update
);

module.exports = router;
