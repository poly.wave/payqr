const express = require('express');
const validate = require('express-validation');
const router = express.Router();
const upload = require('../../../helpers/multer');

const {
  merchantValidator
} = require('../../../validators');

const {
  checkPermission
} = require('../../../middlewares/merchant/currentMerchantMiddleware');

const controller = require('../../../controllers/merchant/merchantController');

//Validation
validate.options({
  allowUnknownBody: false,
  allowUnknownHeaders: false,
  allowUnknownQuery: false,
  allowUnknownParams: false,
  allowUnknownCookies: false
});

router.get(
  '/show',
  validate(merchantValidator.show),
  checkPermission(['admin', 'manager', 'user']),
  controller.show
);

router.get(
  '/showall',
  controller.showall
);

router.get(
  '/remove',
  validate(merchantValidator.remove),
  checkPermission(['admin', 'manager']),
  controller.remove
);

router.post(
  '/create',
  validate(merchantValidator.create),
  checkPermission(['admin', 'manager']),
  controller.create
);

router.post(
  '/update',
  validate(merchantValidator.update),
  checkPermission(['admin', 'manager']),
  controller.update
);

router.patch(
  '/update/avatar',
  validate(merchantValidator.update),
  checkPermission(['admin', 'manager']),
  upload.single("avatar"),
  controller.updateAvatar
);

module.exports = router;
