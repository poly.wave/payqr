const express = require('express');
const router = express.Router();
const validate = require('express-validation');
const upload = require('../../../helpers/multer');

const {
  merchantProductValidator
} = require('../../../validators');

const {
  checkPermission
} = require('../../../middlewares/merchant/currentMerchantMiddleware');

const controller = require('../../../controllers/merchant/merchantProductController');

//Validation
validate.options({
  allowUnknownBody: false,
  allowUnknownHeaders: false,
  allowUnknownQuery: false,
  allowUnknownParams: false,
  allowUnknownCookies: false
});


router.get(
  '/show',
  validate(merchantProductValidator.show),
  checkPermission(['admin', 'manager', 'user']),
  controller.show
);


router.get(
  '/showall',
  validate(merchantProductValidator.showall),
  checkPermission(['admin', 'manager', 'user']),
  controller.showall
);

router.get(
  '/remove',
  validate(merchantProductValidator.remove),
  checkPermission(['admin', 'manager']),
  controller.remove
);

router.post(
  '/create',
  validate(merchantProductValidator.create),
  checkPermission(['admin', 'manager']),
  controller.create
);

router.post(
  '/update',
  validate(merchantProductValidator.update),
  checkPermission(['admin', 'manager']),
  controller.update
);

router.patch(
  '/update/image',
  validate(merchantProductValidator.update),
  checkPermission(['admin', 'manager']),
  upload.single("image"),
  controller.updateImage
);

module.exports = router;
