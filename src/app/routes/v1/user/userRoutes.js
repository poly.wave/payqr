const express = require('express');
const router = express.Router();
const validate = require('express-validation');
const upload = require('../../../helpers/multer.js');

const {
  userValidator
} = require('../../../validators');

//Validation
validate.options({
  allowUnknownBody: false,
  allowUnknownHeaders: false,
  allowUnknownQuery: false,
  allowUnknownParams: false,
  allowUnknownCookies: false
});


const controller = require('../../../controllers/user/userController');

router.get(
  '/',
  controller.show
);

router.post(
  '/update',
  validate(userValidator.update),
  controller.update
);

router.patch(
  '/update/avatar',
  validate(userValidator.updateAvatar),
  upload.single("avatar"),
  controller.updateAvatar
);

module.exports = router;
