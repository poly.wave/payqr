const express = require('express');
const router = express.Router();
const validate = require('express-validation');

const {
  userCardValidator
} = require('../../../validators');

//Validation
validate.options({
  allowUnknownBody: false,
  allowUnknownHeaders: false,
  allowUnknownQuery: false,
  allowUnknownParams: false,
  allowUnknownCookies: false
});


const controller = require('../../../controllers/user/userCardController');

router.get(
  '/show',
  validate(userCardValidator.show),
  controller.show
);

router.get(
  '/showall',
  controller.showall
);

router.get(
  '/remove',
  validate(userCardValidator.remove),
  controller.remove
);

router.post(
  '/create',
  validate(userCardValidator.create),
  controller.create
);

module.exports = router;
