const fs = require('fs');

fs.readdirSync(__dirname + '/').filter(file =>
  (file.indexOf('.') !== 0) &&
  (file.indexOf('index') !== 0)
).forEach(function(dir) {
  fs.readdirSync(__dirname + '/' + dir).filter(file =>
    (file.indexOf('index') !== 0)
  ).forEach(function(file) {
    let name = file.replace('.js', '');

    exports[name] = require('./' + dir + '/' + file);
  });
});

module.exports = exports;
