const express = require('express');
const router = express.Router();
const validate = require('express-validation');

const {
  transactionValidator
} = require('../../../validators');

const {
  checkPermission
} = require('../../../middlewares/merchant/currentMerchantMiddleware');

const controller = require('../../../controllers/transaction/transactionController');

//Validation
validate.options({
  allowUnknownBody: false,
  allowUnknownHeaders: false,
  allowUnknownQuery: false,
  allowUnknownParams: false,
  allowUnknownCookies: false
});

router.get(
  '/show',
  validate(transactionValidator.show),
  checkPermission(['admin', 'manager', 'user']),
  controller.show
);

router.get(
  '/showall',
  validate(transactionValidator.showall),
  checkPermission(['admin', 'manager', 'user']),
  controller.showall
);

router.get(
  '/remove',
  validate(transactionValidator.update),
  checkPermission(['admin', 'manager']),
  controller.remove
);

router.post(
  '/update',
  validate(transactionValidator.update),
  checkPermission(['admin', 'manager']),
  controller.update
);

module.exports = router;
