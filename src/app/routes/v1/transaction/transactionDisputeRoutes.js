const express = require('express');
const router = express.Router();
const validate = require('express-validation');

const {
  transactionDisputeValidator
} = require('../../../validators');

const {
  checkPermission
} = require('../../../middlewares/merchant/currentMerchantMiddleware');

const controller = require('../../../controllers/transaction/transactionDisputeController');

//Validation
validate.options({
  allowUnknownBody: false,
  allowUnknownHeaders: false,
  allowUnknownQuery: false,
  allowUnknownParams: false,
  allowUnknownCookies: false
});

router.get(
  '/show',
  validate(transactionDisputeValidator.show),
  checkPermission(['admin', 'manager', 'user']),
  controller.show
);

router.get(
  '/showall',
  validate(transactionDisputeValidator.showall),
  checkPermission(['admin', 'manager', 'user']),
  controller.showall
);

router.get(
  '/remove',
  validate(transactionDisputeValidator.remove),
  checkPermission(['admin', 'manager']),
  controller.remove
);

module.exports = router;
