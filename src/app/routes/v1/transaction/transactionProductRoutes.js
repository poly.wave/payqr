const express = require('express');
const router = express.Router();
const validate = require('express-validation');

const {
  transactionProductValidator
} = require('../../../validators');

const {
  checkPermission
} = require('../../../middlewares/merchant/currentMerchantMiddleware');

const controller = require('../../../controllers/transaction/transactionProductController');

//Validation
validate.options({
  allowUnknownBody: false,
  allowUnknownHeaders: false,
  allowUnknownQuery: false,
  allowUnknownParams: false,
  allowUnknownCookies: false
});

router.get(
  '/show',
  validate(transactionProductValidator.show),
  checkPermission(['admin', 'manager', 'user']),
  controller.show
);

router.get(
  '/showall',
  validate(transactionProductValidator.showall),
  checkPermission(['admin', 'manager', 'user']),
  controller.showall
);

router.get(
  '/remove',
  validate(transactionProductValidator.remove),
  checkPermission(['admin', 'manager']),
  controller.remove
);

module.exports = router;
