const underscore = require('underscore');
const httpStatus = require('http-status');

const {
  APIError
} = require('../../helpers');

const {
  Transaction
} = require('../../models');

let middleware = {};

middleware.currentTransaction = () => {
  return (req, res, next) => {
    if(req.body.transaction_id && !req.currentTransaction) {
      Transaction.cache().findById(req.body.transaction_id).then((transaction) => {
        if (!transaction) return next(new APIError('Transaction not found', httpStatus.NOT_FOUND));

        req.currentTransaction = transaction;
      }).then(() => {
        if(req.body.transaction_product_id && !req.currentTransactionProduct) {
          TransactionProduct.cache().findById(req.body.transaction_product_id).then((transactionProduct) => {
            if (!transactionProduct) return next(new APIError('Transaction product not found', httpStatus.NOT_FOUND));

            req.currentTransactionProduct = transactionProduct;

            next();
          }).catch(err => {
            return next(new APIError(err, httpStatus.NOT_FOUND));
          });
        } else {
          next();
        }
      }).catch(err => {
        return next(new APIError(err, httpStatus.NOT_FOUND));
      });
    } else {
      next();
    }
  };
};

module.exports = middleware;
