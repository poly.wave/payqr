const httpStatus = require('http-status');

const {
  APIError
} = require('../../helpers');

const {
  User,
  UserCard
} = require('../../models');



let middleware = {};

middleware.currentUser = () => {
  return (req, res, next) => {
    if(req.user && req.user.data.id) {
      User.cache().findById(req.user.data.id).then((user) => {
        if (!user) return next(new APIError('User not found', httpStatus.NOT_FOUND));

        req.user = user;
      }).then(() => {
        if(req.body.user_card_id && !req.currentUserCard) {
          UserCard.cache().findById(req.body.user_card_id).then((userCard) => {
            if (!userCard) return next(new APIError('User card not found', httpStatus.NOT_FOUND));

            req.currentUserCard = userCard;

            next();
          }).catch(err => {
            return next(new APIError(err, httpStatus.NOT_FOUND));
          });
        } else {
          next();
        }
      }).catch(err => {
        return next(new APIError(err, httpStatus.NOT_FOUND));
      });
    } else {
      next();
    }
  };
};

module.exports = middleware;
