const httpStatus = require('http-status');

const {
  APIError
} = require('../../helpers');

const {
  Merchant,
  MerchantUser,
  MerchantProduct
} = require('../../models');

let middleware = {};

middleware.currentMerchant = () => {
  return (req, res, next) => {
    if(req.body.merchant_id && !req.currentMerchant) {
      Merchant.cache().findById(req.body.merchant_id).then((merchant) => {
        if (!merchant) return next(new APIError('Merchant not found', httpStatus.NOT_FOUND));
        req.currentMerchant = merchant;
      }).then(() => {
        if(req.body.merchant_product_id && !req.currentMerchantProduct) {
          MerchantProduct.cache().findById(req.body.merchant_product_id).then((merchantProduct) => {
            if (!merchantProduct) return next(new APIError('Merchant product not found', httpStatus.NOT_FOUND));
            req.currentMerchantProduct = merchantProduct;
            next();
          }).catch(err => {
            return next(new APIError(err, httpStatus.NOT_FOUND));
          });
        } else {
          next();
        }
      }).catch(err => {
        return next(new APIError(err, httpStatus.NOT_FOUND));
      });
    } else {
      next();
    }
  };
};

middleware.checkPermission = (permissions = []) => {
  if (typeof permissions == 'string') permissions = [permissions];
  return (req, res, next) => {
    if (!req.currentMerchant) return next(new APIError('Merchant not found', httpStatus.NOT_FOUND));
    MerchantUser.findOne({
      where: {
        merchant_id: req.currentMerchant.id,
        user_id: req.user.id,
        permission: permissions
      }
    }).then(result => {
      if (!result) return next(new APIError('Not found or you do not have access!', httpStatus.NOT_FOUND));
      next();
    }).catch(err => {
      next(new APIError(err, httpStatus.NOT_FOUND));
    });
  };
};

module.exports = middleware;
