const httpStatus = require('http-status');

const {
  APIError,
  stripe
} = require('../../helpers');

const {
  UserCard
} = require('../../models');


var updateUserPaymentDefault = function (user, def) {
  if (!def) return true;
  UserCard.update({ default: false }, {where: { user_id: user }}).then((result) => {
    return true;
  }).catch(e => {
    return false;
  });
};

let controller = {};

controller.show = (req, res, next) => {
  UserCard.findOne({
    id: req.body.user_card_id,
    user_id: req.user.id
  }).then(result => {
    return res.json({
      public: true,
      res: result
    });
  });
};

controller.showall = (req, res) => {
  UserCard.findAll({
    where: {
      user_id: req.user.id
    }
  }).then(result => {
    return res.json({
      public: true,
      res: result
    });
  });
};

controller.remove = (req, res, next) => {
  // Delete card
  stripe.customers.deleteCard(req.user.stripe_id, req.body.token).then((source) => {
    if(source.deleted == false) return next(new APIError('Stripe card deleting error', httpStatus.NOT_FOUND));
    UserCard.delete({
      where: {
        id: source.id,
        user_id: req.user.id
      }
    }).then(result => {
      return res.json({
        public: true,
        res: result
      });
    }).catch(function(err){
      return next(new APIError(err, httpStatus.NOT_FOUND));
    });
  }).catch((err) => {
    return next(new APIError("Stripe card deleting error: " + err, httpStatus.NOT_FOUND));
  });
};

controller.create = (req, res, next) => {
  if(updateUserPaymentDefault(req.user.id, req.body.default) == false) return next(new APIError("Update all Defaults, ERROR", httpStatus.NOT_FOUND));
  stripe.customers.createSource(req.user.stripe_id, {
    source: req.body.token
    //TODO ADD FULL ADDRESS, FULL NAME, CANT CREATE CARD WITHOUT ADDRESS AND FULL PROFILE
  }).then((source) => {
    UserCard.create({
      token: source.id,
      fingerprint: source.fingerprint,
      last4: source.last4,
      brand: source.brand,
      exp_month: source.exp_month,
      exp_year: source.exp_year,
      default: req.body.default,
      user_id: req.user.id
    }).then(result => {
      if(req.body.default)
        stripe.customers.update(req.user.stripe_id, {
          source: source.id,
        }).catch((err) => {
          return next(new APIError("Stripe user card updating error: " + err, httpStatus.NOT_FOUND));
        });
    }).then(result => {
      return res.json({ success: true, res: result });
    }).catch(function(err){
      return next(new APIError(err, httpStatus.NOT_FOUND));
    });
  }).catch((err) => {
    return next(new APIError("Stripe card creating error: " + err, httpStatus.NOT_FOUND));
  });
};

module.exports = controller;
