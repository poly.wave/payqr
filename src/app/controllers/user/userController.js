const httpStatus = require('http-status');

const {
  APIError,
  s3
} = require('../../helpers');

const {
  User
} = require('../../models');



let controller = {};

controller.show = (req, res, next) => {
  return res.json({
    user: req.user
  });
};

controller.update = (req, res, next) => {
  User.update(req.body, {
    where: {
      id: req.user.id
    },
    returning: true,
    plain: true
  }).then((result) => {
    return res.json({
      success: true,
      res: result
    });
  }).catch(e => {
    return next(new APIError(e, httpStatus.NOT_FOUND));
  });
};

controller.updateAvatar = (req, res, next) => {
	s3.avatarParams.Key = "avatars/user/id_" + req.user.id + ".jpg";
	s3.avatarParams.Body = req.file.buffer;

	s3.s3Client.upload(s3.avatarParams, (err, data) => {
	  if (err) return next(new APIError(err, httpStatus.NOT_FOUND));

    User.cache().update({
      avatarUrl: '/avatars/user/id_' + req.user.id + '.jpg'
    }, {
      where: {
        id: req.user.id
      },
      returning: true,
      plain: true
    }).then((result) => {
      return res.json({
        success: true,
        res: result
      });
    }).catch(e => {
      return next(new APIError(e, httpStatus.NOT_FOUND));
    });
	});
};

module.exports = controller;
