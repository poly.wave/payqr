const httpStatus = require('http-status');
const crypto = require('crypto');

const {
  APIError,
  s3
} = require('../../helpers');

const {
  Merchant,
  MerchantUser
} = require('../../models');



let controller = {};

controller.show = (req, res) => {
  Merchant.findById(req.body.merchant_id).then(result => {
    return res.json({
      public: true,
      res: result
    });
  });
};

controller.showall = (req, res) => {
  Merchant.findAll().then(result => {
    return res.json({
      public: true,
      res: result
    });
  });
};

controller.remove = (req, res, next) => {
  Merchant.delete({
    where: {
      id: req.body.merchant_id
    }
  }).then(result => {
    return res.json({
      public: true,
      res: result
    });
  }).catch(function(err){
    return next(new APIError(err, httpStatus.NOT_FOUND));
  });
};

controller.create = (req, res, next) => {
  let current_date = (new Date()).valueOf().toString();
  let hash = crypto.createHash('sha256')
    .update(current_date + Math.random().toString())
    .digest('hex');
  let key = crypto.createHash('md5')
    .update(current_date + Math.random().toString())
    .digest('hex');

  let createdMerchant = undefined;

  Merchant.create({
    legalName: req.body.legalName,
    apiHash: hash,
    apiSecret: key
  }).then(merchant => {
    createdMerchant = merchant;

    return MerchantUser.create({
      user_id: req.user.id,
      merchant_id: createdMerchant.id,
      permission: 'admin'
    });
  }).then(createdMerchantUser => {
    return res.json({
      success: true,
      res: {
        Merchant: createdMerchant,
        MerchantUser: createdMerchantUser
      }
    });
  }).catch(function(err){
     return next(new APIError(err, httpStatus.NOT_FOUND));
  });
};

controller.update = (req, res, next) => {
  Merchant.update(req.body, {
    where: {
      id: req.body.merchant_id
    },
    returning: true,
    plain: true
  }).then((result) => {
    return res.json({
      success: true,
      res: result
    });
  }).catch(err => {
    return next(new APIError(err, httpStatus.NOT_FOUND));
  });
};

controller.updateAvatar = (request, res, next) => {
  let avatar = "/avatars/merchant/id_" + request.user.id + ".jpg";

	s3.avatarParams.Key = avatar;
	s3.avatarParams.Body = request.file.buffer;

	s3.s3Client.upload(s3.avatarParams, (err, data) => {
  	if (err) return next(new APIError(err, httpStatus.NOT_FOUND));

    Merchant.update({
      avatarUrl: avatar
    }, {
      where: {
        id: request.body.merchant_id
      },
      returning: true,
      plain: true
    }).then((result) => {
      return res.json({
        success: true,
        res: result
      });
    }).catch(e => {
      return next(new APIError(e, httpStatus.NOT_FOUND));
    });
	});
};

module.exports = controller;
