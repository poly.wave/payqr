const httpStatus = require('http-status');

const {
  APIError,
  s3
} = require('../../helpers');

const {
  MerchantProduct
} = require('../../models');


let controller = {};

controller.show = (req, res) => {
  MerchantProduct.findById(req.body.merchant_product_id).then(result => {
    return res.json({
      public: true,
      res: result
    });
  });
};

controller.showall = (req, res) => {
  MerchantProduct.findAll({
    where: {
      merchant_id: req.currentMerchant.id
    }
  }).then(result => {
    return res.json({
      public: true,
      res: result
    });
  });
};

controller.remove = (req, res, next) => {
  MerchantProduct.delete({
    where: {
      id: req.body.merchant_product_id
    }
  }).then(result => {
    return res.json({
      public: true,
      res: result
    });
  }).catch(function(err){
    return next(new APIError(err, httpStatus.NOT_FOUND));
  });
};

controller.create = (req, res, next) => {
  MerchantProduct.create(req.body).then(result => {
    return res.json({
      success: true,
      res: result
    });
  }).catch(function(err){
    return next(new APIError(err, httpStatus.NOT_FOUND));
  });
};

controller.update = (req, res, next) => {
  MerchantProduct.update(req.body, {
    where: {
      id: req.body.merchant_product_id
    },
    returning: true,
    plain: true
  }).then((result) => {
    return res.json({
      success: true,
      res: result
    });
  }).catch(err => {
    return next(new APIError(err, httpStatus.NOT_FOUND));
  });
};

controller.updateImage = (req, res, next) => {
  let image = "images/merchant_product/id_" + req.body.product_id + ".jpg";

  s3.avatarParams.Key = image;
  s3.avatarParams.Body = req.file.buffer;

  s3.s3Client.upload(s3.avatarParams, (err, data) => {
    if (err) return next(new APIError(err, httpStatus.NOT_FOUND));

    MerchantProduct.update({
      imageUrl: image
    }, {
      where: {
        id: req.body.merchant_product_id
      },
      returning: true,
      plain: true
    }).then((result) => {
      return res.json({
        success: true,
        res: result
      });
    }).catch(err => {
      return next(new APIError(err, httpStatus.NOT_FOUND));
    });
  });
};

module.exports = controller;
