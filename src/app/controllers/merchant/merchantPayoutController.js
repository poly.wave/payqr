const httpStatus = require('http-status');

const {
  APIError,
  s3
} = require('../../helpers');

const {
  MerchantPayout
} = require('../../models');


let controller = {};

controller.show = (req, res) => {
  MerchantPayout.findById(req.body.merchant_payout_id).then(result => {
    return res.json({
      public: true,
      res: result
    });
  });
};

controller.showall = (req, res) => {
  MerchantPayout.findAll({
    where: {
      merchant_id: req.currentMerchant.id
    }
  }).then(result => {
    return res.json({
      public: true,
      res: result
    });
  });
};

controller.remove = (req, res, next) => {
  MerchantPayout.delete({
    where: {
      id: req.body.merchant_payout_id
    }
  }).then(result => {
    return res.json({
      public: true,
      res: result
    });
  }).catch(function(err){
    return next(new APIError(err, httpStatus.NOT_FOUND));
  });
};

controller.create = (req, res, next) => {
  MerchantPayout.create(req.body).then(result => {
    return res.json({
      success: true,
      res: result
    });
  }).catch(function(err){
    return next(new APIError(err, httpStatus.NOT_FOUND));
  });
};

controller.update = (req, res, next) => {
  MerchantPayout.update(req.body, {
    where: {
      id: req.body.merchant_payout_id
    },
    returning: true,
    plain: true
  }).then((result) => {
    return res.json({
      success: true,
      res: result
    });
  }).catch(err => {
    return next(new APIError(err, httpStatus.NOT_FOUND));
  });
};

module.exports = controller;
