const httpStatus = require('http-status');

const {
  APIError,
  s3
} = require('../../helpers');

const {
  MerchantProductInventory
} = require('../../models');


let controller = {};

controller.show = (req, res) => {
  MerchantProductInventory.findById(req.body.merchant_product_inventory_id).then(result => {
    return res.json({
      public: true,
      res: result
    });
  });
};

controller.showall = (req, res) => {
  MerchantProductInventory.findAll({
    where: {
      merchant_product_id: req.currentMerchantProduct.id
    }
  }).then(result => {
    return res.json({
      public: true,
      res: result
    });
  });
};

controller.remove = (req, res, next) => {
  MerchantProductInventory.delete({
    where: {
      id: req.body.merchant_product_inventory_id
    }
  }).then(result => {
    return res.json({
      public: true,
      res: result
    });
  }).catch(function(err){
    return next(new APIError(err, httpStatus.NOT_FOUND));
  });
};

controller.create = (req, res, next) => {
  MerchantProductInventory.create(req.body).then(result => {
    return res.json({
      success: true,
      res: result
    });
  }).catch(function(err){
    return next(new APIError(err, httpStatus.NOT_FOUND));
  });
};

controller.update = (req, res, next) => {
  MerchantProductInventory.update(req.body, {
    where: {
      id: req.body.merchant_product_inventory_id
    },
    returning: true,
    plain: true
  }).then((result) => {
    return res.json({
      success: true,
      res: result
    });
  }).catch(err => {
    return next(new APIError(err, httpStatus.NOT_FOUND));
  });
};

controller.updateImage = (req, res, next) => {
  let image = "images/merchant_product_inventory/id_" + req.body.inventory_id + ".jpg";

  s3.avatarParams.Key = image;
  s3.avatarParams.Body = req.file.buffer;

  s3.s3Client.upload(s3.avatarParams, (err, data) => {
    if (err) return next(new APIError(err, httpStatus.NOT_FOUND));

    MerchantProductInventory.update({
      imageUrl: image
    }, {
      where: {
        id: req.body.merchant_product_inventory_id
      },
      returning: true,
      plain: true
    }).then((result) => {
      return res.json({
        success: true,
        res: result
      });
    }).catch(err => {
      return next(new APIError(err, httpStatus.NOT_FOUND));
    });
  });
};

module.exports = controller;
