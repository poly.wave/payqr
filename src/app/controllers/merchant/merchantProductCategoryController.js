const httpStatus = require('http-status');

const {
  APIError
} = require('../../helpers');

const {
  MerchantProductCategory
} = require('../../models');


let controller = {};

controller.show = (req, res) => {
  MerchantProductCategory.findById(req.body.merchant_product_category_id).then(result => {
    return res.json({
      public: true,
      res: result
    });
  });
};

controller.showall = (req, res) => {
  MerchantProductCategory.findAll({
    where: {
      merchant_id: req.currentMerchant.id
    }
  }).then(result => {
    return res.json({
      public: true,
      res: result
    });
  });
};

controller.remove = (req, res, next) => {
  MerchantProductCategory.delete({
    where: {
      id: req.body.merchant_product_category_id
    }
  }).then(result => {
    return res.json({
      public: true,
      res: result
    });
  }).catch(function(err){
    return next(new APIError(err, httpStatus.NOT_FOUND));
  });
};

controller.create = (req, res, next) => {
  MerchantProductCategory.create(req.body).then(result => {
    return res.json({
      success: true,
      res: result
    });
  }).catch(function(err){
    return next(new APIError(err, httpStatus.NOT_FOUND));
  });
};

controller.update = (req, res, next) => {
  MerchantProductCategory.update(req.body, {
    where: {
      id: req.body.merchant_product_category_id
    },
    returning: true,
    plain: true
  }).then((result) => {
    return res.json({
      success: true,
      res: result
    });
  }).catch(err => {
    return next(new APIError(err, httpStatus.NOT_FOUND));
  });
};

module.exports = controller;
