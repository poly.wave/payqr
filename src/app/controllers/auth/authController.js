const config = require('../../../config/env');

const jwt = require('jsonwebtoken');
const httpStatus = require('http-status');
const randomNumber = require("random-number-csprng");
const tw = require('twilio')(config.twilio.sid, config.twilio.token);

const {
  APIError,
  redis,
  stripe
} = require('../../helpers');

const {
  User,
  UserSetting
} = require('../../models');

let sendSms = false;
let controller = {};

controller.login = (req, res, next) => {
  const i18n = req.app.locals.i18n;
  if(req.body.pin){
    redis.get("auth_code_" + req.body.mobileNumber, function(err, reply) {
      if (err) return next(new APIError(err, httpStatus.NOT_FOUND));
      if (req.body.pin != reply) return next(new APIError(i18n.ERROR_PIN, httpStatus.NOT_FOUND));
      User.findOne({
        where: {
          mobileNumber: req.body.mobileNumber
        }
      }).then((user) => {
        if (user) {
          const token = jwt.sign({exp: Math.floor(Date.now() / 1000) + (60 * 43800),data: {id: user.id,phone: user.mobileNumber}}, config.jwtSecret);
          return res.json({success: true,user: user,session: token});
        } else {
          stripe.customers.create({
            metadata: {
              mobileNumber: req.body.mobileNumber
            }
          }).then((stripe) => {
            User.create({
              mobileNumber: req.body.mobileNumber,
              stripe_id: stripe.id
            }).then(_user => {
              const token = jwt.sign({exp: Math.floor(Date.now() / 1000) + (60 * 43800),data: {id: _user.id,phone: _user.mobileNumber}}, config.jwtSecret);
              return res.json({success: true,user: _user,session: token});
            });
          });
        }
      });
    });
  } else {
    Promise.try(() => {
      return randomNumber(100000, 999999);
    }).then((pin) => {
      redis.ttl("auth_code_" + req.body.mobileNumber, (err, result) => {
        if (err) return next(new APIError(err, httpStatus.NOT_FOUND));
        if(result != -2) return next(new APIError(i18n.ERROR_PIN_NOT_EXPIRED + result + " seconds", httpStatus.NOT_FOUND));
        redis.setex("auth_code_" + req.body.mobileNumber, 10, pin, function(err, result) {
          if (err) return next(new APIError(err, httpStatus.NOT_FOUND));
          if(sendSms === true) {
            tw.messages.create({
              from: config.twilio.number,
              body: "SkyQR: Your security code is: " + pin + " Your code expires in 5 minutes. Please don't reply.",
              to: req.body.mobileNumber
            }).then(message => console.log(message.sid)).done();
          }
          return res.json({
            success: true,
            res: 'Pin for test: ' + pin
          });
        });
      });
    });
  }
}

module.exports = controller;
