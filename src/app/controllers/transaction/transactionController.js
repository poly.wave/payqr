const httpStatus = require('http-status');

const {
  APIError
} = require('../../helpers');

const {
  Transaction
} = require('../../models');



let controller = {};

controller.show = (req, res, next) => {
  Transaction.findById(req.body.transaction_id).then(result => {
    return res.json({
      public: true,
      res: result
    });
  });
};

controller.showall = (req, res) => {
  Transaction.findAll({
    where: {
      user_id: req.user.id
    }
  }).then(result => {
    return res.json({
      public: true,
      res: result
    });
  });
};

controller.remove = (req, res, next) => {
  Transaction.delete({
    where: {
      id: req.body.transaction_id
    }
  }).then(result => {
    return res.json({
      public: true,
      res: result
    });
  }).catch(function(err){
    return next(new APIError(err, httpStatus.NOT_FOUND));
  });
};

controller.create = (req, res, next) => {
  Transaction.create(req.body).then(result => {
    return res.json({
      success: true,
      res: result
    });
  }).catch(function(err){
    return next(new APIError(err, httpStatus.NOT_FOUND));
  });
};

controller.update = (req, res, next) => {
  Transaction.update(req.body, {
    where: {
      id: req.body.transaction_id
    },
    returning: true,
    plain: true
  }).then((result) => {
    return res.json({
      success: true,
      res: result
    });
  }).catch(e => {
    return next(new APIError(e, httpStatus.NOT_FOUND));
  });
};

module.exports = controller;
