const httpStatus = require('http-status');

const {
  APIError
} = require('../../helpers');

const {
  TransactionProduct
} = require('../../models');



let controller = {};

controller.show = (req, res, next) => {
  TransactionProduct.findById(req.body.transaction_product_id).then(result => {
    return res.json({
      public: true,
      res: result
    });
  });
};

controller.showall = (req, res) => {
  TransactionProduct.findAll({
    where: {
      transaction_id: req.currentTransaction.id
    }
  }).then(result => {
    return res.json({
      public: true,
      res: result
    });
  });
};

controller.remove = (req, res, next) => {
  TransactionProduct.delete({
    where: {
      id: req.body.transaction_product_id
    }
  }).then(result => {
    return res.json({
      public: true,
      res: result
    });
  }).catch(function(err){
    return next(new APIError(err, httpStatus.NOT_FOUND));
  });
};

controller.create = (req, res, next) => {
  TransactionProduct.create(req.body).then(result => {
    return res.json({
      success: true,
      res: result
    });
  }).catch(function(err){
    return next(new APIError(err, httpStatus.NOT_FOUND));
  });
};

controller.update = (req, res, next) => {
  TransactionProduct.update(req.body, {
    where: {
      id: req.body.transaction_product_id
    },
    returning: true,
    plain: true
  }).then((result) => {
    return res.json({
      success: true,
      res: result
    });
  }).catch(e => {
    return next(new APIError(e, httpStatus.NOT_FOUND));
  });
};

module.exports = controller;
