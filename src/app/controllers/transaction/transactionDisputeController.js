const httpStatus = require('http-status');

const {
  APIError
} = require('../../helpers');

const {
  TransactionDispute
} = require('../../models');



let controller = {};

controller.show = (req, res, next) => {
  TransactionDispute.findById(req.body.transaction_dispute_id).then(result => {
    return res.json({
      public: true,
      res: result
    });
  });
};

controller.showall = (req, res) => {
  TransactionDispute.findAll({
    where: {
      transaction_id: req.currentTransaction.id
    }
  }).then(result => {
    return res.json({
      public: true,
      res: result
    });
  });
};

controller.remove = (req, res, next) => {
  TransactionDispute.delete({
    where: {
      id: req.body.transaction_dispute_id
    }
  }).then(result => {
    return res.json({
      public: true,
      res: result
    });
  }).catch(function(err){
    return next(new APIError(err, httpStatus.NOT_FOUND));
  });
};

controller.create = (req, res, next) => {
  TransactionDispute.create(req.body).then(result => {
    return res.json({
      success: true,
      res: result
    });
  }).catch(function(err){
    return next(new APIError(err, httpStatus.NOT_FOUND));
  });
};

controller.update = (req, res, next) => {
  TransactionDispute.update(req.body, {
    where: {
      id: req.body.transaction_dispute_id
    },
    returning: true,
    plain: true
  }).then((result) => {
    return res.json({
      success: true,
      res: result
    });
  }).catch(e => {
    return next(new APIError(e, httpStatus.NOT_FOUND));
  });
};

module.exports = controller;
