const config = require('../../config/env');

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const RedisAdaptor = require('sequelize-transparent-cache-ioredis');
const sequelizeCache = require('sequelize-transparent-cache');

const redis = require("../helpers/redis");

const redisAdaptor = new RedisAdaptor({
  client: redis,
  namespace: 'model',
  lifetime: 60 * 60
});

const { withCache } = sequelizeCache(redisAdaptor);

let db = {};

let sequelize = new Sequelize(config.database.dbname, config.database.username, config.database.password, {
  host: config.database.host,
  dialect: config.database.dialect,
  operatorsAliases: false,
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
});

sequelize.authenticate().then(() => {
  console.log('Database connection has been established successfully.');
}).catch(err => {
  console.error('Unable to connect to the database:', err);
});

fs.readdirSync(__dirname).filter(file =>
  (file.indexOf('.') !== 0) &&
  (file.indexOf('index') !== 0)
).forEach((dir) => {
  fs.readdirSync(__dirname + '/' + dir).filter(file =>
    (file.indexOf('index') !== 0)
  ).forEach((file) => {
    const model = sequelize.import(path.join(__dirname + '/' + dir, file));

    db[model.name] = model;
  });
});

Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }

  withCache(db[modelName]);
});

db.sequelize = sequelize;

module.exports = db;

