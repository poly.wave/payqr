'use strict';
module.exports = (sequelize, DataTypes) => {
  const Transaction = sequelize.define('Transaction', {
    description: DataTypes.TEXT,
    type: {
      type: DataTypes.ENUM,
      values: ['static', 'dynamic']
    },
    status: {
      type: DataTypes.ENUM,
      values: ['new', 'pending', 'complete', 'rejected', 'canceled', 'refunded_p', 'refunded'],
      defaultValue: 'new'
    },
    amount: DataTypes.DECIMAL(12,2),
    currency: DataTypes.STRING,
    paid: DataTypes.BOOLEAN,
    fee: DataTypes.DECIMAL(12,2),
    livemode: DataTypes.BOOLEAN,
    source_id:DataTypes.INTEGER,
    dispute_id:DataTypes.INTEGER,
    merchant_id: DataTypes.UUID,
    user_id: DataTypes.UUID,
    cashier_id: DataTypes.UUID,
    expires_at: DataTypes.DATE
  }, {});

  Transaction.associate = function(models) {
    Transaction.hasMany(models.TransactionProduct, {
      foreignKey: 'transaction_id'
    });
    Transaction.hasMany(models.TransactionDispute, {
      foreignKey: 'transaction_id'
    });
    Transaction.belongsTo(models.User, {
      foreignKey:'user_id'
    });
    Transaction.belongsTo(models.Merchant, {
      foreignKey:'merchant_id'
    });
  };

  return Transaction;
};
