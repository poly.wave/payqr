'use strict';
module.exports = (sequelize, DataTypes) => {
  const TransactionDispute = sequelize.define('TransactionDispute', {
    transaction_id: DataTypes.INTEGER
  }, {});

  TransactionDispute.associate = function(models) {
    TransactionDispute.belongsTo(models.Transaction, {
      foreignKey:'transaction_id'
    });
  };

  return TransactionDispute;
};
