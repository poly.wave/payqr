'use strict';
module.exports = (sequelize, DataTypes) => {
  const TransactionProduct = sequelize.define('TransactionProduct', {
    merchant_product_id: DataTypes.INTEGER,
    transaction_id: DataTypes.INTEGER
  }, {});

  TransactionProduct.associate = function(models) {
    TransactionProduct.belongsTo(models.MerchantProduct, {
      foreignKey:'merchant_product_id'
    });
    TransactionProduct.belongsTo(models.Transaction, {
      foreignKey:'transaction_id'
    });
  };

  return TransactionProduct;
};
