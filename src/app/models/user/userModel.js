'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    mobileNumber: DataTypes.STRING,
    email: DataTypes.STRING,
    ip: DataTypes.STRING,
    role: {
      type: DataTypes.ENUM,
      values: ['user', 'manager', 'admin'],
      defaultValue: 'user'
    },
    avatarUrl: DataTypes.STRING,
    active: DataTypes.BOOLEAN,
    stripe_id: DataTypes.STRING
  }, {});

  User.associate = function(models) {
    User.belongsToMany(models.Merchant, {
      through: models.MerchantUser,
      foreignKey: 'user_id'
    });
    User.hasMany(models.Transaction, {
      foreignKey: 'user_id',
      as: 'transactions'
    });
    User.hasMany(models.UserCard, {
      foreignKey: 'user_id',
      as: 'cards'
    });
    User.hasMany(models.UserSetting, {
      foreignKey: 'user_id',
      as: 'settings'
    });
  };

  return User;
};
