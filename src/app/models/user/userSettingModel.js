'use strict';
module.exports = (sequelize, DataTypes) => {
  const UserSetting = sequelize.define('UserSetting', {
    name: DataTypes.STRING,
    value: DataTypes.STRING,
    user_id: DataTypes.INTEGER
  }, {});

  UserSetting.associate = function(models) {
    UserSetting.belongsTo(models.User, {
      foreignKey: 'user_id'
    });
  };

  return UserSetting;
};
