'use strict';
module.exports = (sequelize, DataTypes) => {
  const UserCard = sequelize.define('UserCard', {
    token: DataTypes.STRING,
    fingerprint: DataTypes.STRING,
    last4: DataTypes.INTEGER,
    brand: DataTypes.STRING,
    exp_month: DataTypes.INTEGER,
    exp_year: DataTypes.INTEGER,
    default: DataTypes.BOOLEAN,
    user_id: DataTypes.INTEGER
  }, {});

  UserCard.associate = function(models) {
    UserCard.belongsTo(models.User, {
      foreignKey:'user_id'
    });
  };

  return UserCard;
};
