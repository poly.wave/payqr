'use strict';
module.exports = (sequelize, DataTypes) => {
  const MerchantProduct = sequelize.define('MerchantProduct', {
    name: DataTypes.STRING,
    caption: DataTypes.STRING,
    description: DataTypes.STRING,
    url: DataTypes.STRING,
    tags: DataTypes.TEXT,
    imageUrl: DataTypes.STRING,
    active: DataTypes.BOOLEAN,
    category_id: DataTypes.INTEGER,
    merchant_id: DataTypes.UUID
  }, {});

  MerchantProduct.associate = function(models) {
    MerchantProduct.hasMany(models.MerchantProductMetadata, {
      foreignKey: 'merchant_product_id'
    });
    MerchantProduct.hasMany(models.MerchantProductInventory, {
      foreignKey: 'merchant_product_id'
    });
    MerchantProduct.belongsTo(models.Merchant, {
      foreignKey:'merchant_id'
    });
    MerchantProduct.belongsTo(models.MerchantProductCategory, {
      foreignKey:'category_id'
    });
  };

  return MerchantProduct;
};
