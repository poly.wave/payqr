'use strict';
module.exports = (sequelize, DataTypes) => {
  const MerchantPayout = sequelize.define('MerchantPayout', {
    stripe_id: DataTypes.STRING,
    amount: DataTypes.INTEGER,
    destination: DataTypes.STRING,
    arrival_date: DataTypes.DATE,
    created_date: DataTypes.DATE,
    merchant_id: DataTypes.INTEGER
  }, {});

  MerchantPayout.associate = function(models) {
    MerchantPayout.belongsTo(models.Merchant, {
      foreignKey:'merchant_id'
    });
  };

  return MerchantPayout;
};
