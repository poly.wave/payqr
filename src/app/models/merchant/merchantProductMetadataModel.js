'use strict';
module.exports = (sequelize, DataTypes) => {
  const MerchantProductMetadata = sequelize.define('MerchantProductMetadata', {
    key: DataTypes.STRING,
    value: DataTypes.STRING,
    merchant_product_id: DataTypes.INTEGER
  }, {});

  MerchantProductMetadata.associate = function(models) {
    MerchantProductMetadata.belongsTo(models.MerchantProduct, {
      foreignKey: 'merchant_product_id'
    });
  };

  return MerchantProductMetadata;
};
