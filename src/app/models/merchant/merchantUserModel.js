'use strict';
module.exports = (sequelize, DataTypes) => {
  const MerchantUser = sequelize.define('MerchantUser', {
    user_id: DataTypes.INTEGER,
    merchant_id: DataTypes.INTEGER,
    permission: {
      type: DataTypes.ENUM,
      values: ['admin', 'manager', 'user'],
      defaultValue: 'user'
    }
  }, {});

  MerchantUser.associate = function(models) {
    MerchantUser.belongsTo(models.Merchant, {
      foreignKey: 'merchant_id',
      targetKey: 'id'
    });
    MerchantUser.belongsTo(models.User, {
      foreignKey: 'user_id',
      targetKey: 'id'
    });
  };

  return MerchantUser;
};
