'use strict';
module.exports = (sequelize, DataTypes) => {
  const MerchantProductInventory = sequelize.define('MerchantProductInventory', {
    imageUrl: DataTypes.STRING,
    currency: DataTypes.STRING,
    amount: DataTypes.DECIMAL(12,2),
    tags: DataTypes.TEXT,
    inventory: {
      type: DataTypes.ENUM,
      values: ['finite', 'infinite'],
      defaultValue: 'infinite'
    },
    availability: DataTypes.INTEGER,
    active: DataTypes.BOOLEAN,
    merchant_product_id: DataTypes.INTEGER
  }, {});

  MerchantProductInventory.associate = function(models) {
    MerchantProductInventory.belongsTo(models.MerchantProduct, {
      foreignKey: 'merchant_product_id'
    });
  };

  return MerchantProductInventory;
};
