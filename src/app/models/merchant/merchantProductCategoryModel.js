'use strict';
module.exports = (sequelize, DataTypes) => {
  const MerchantProductCategory = sequelize.define('MerchantProductCategory', {
    name: DataTypes.STRING,
    merchant_id: DataTypes.INTEGER
  }, {});

  MerchantProductCategory.associate = function(models) {
    MerchantProductCategory.belongsTo(models.Merchant, {
      foreignKey:'merchant_id'
    });
  };

  return MerchantProductCategory;
};
