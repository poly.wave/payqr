'use strict';
module.exports = (sequelize, DataTypes) => {
  const Merchant = sequelize.define('Merchant', {
    legalName: DataTypes.STRING,
    dbaName: DataTypes.STRING,
    avatarUrl: DataTypes.STRING,
    address_line1: DataTypes.STRING,
    address_line2: DataTypes.STRING,
    address_state: DataTypes.STRING,
    address_zip: DataTypes.STRING,
    address_city: DataTypes.STRING,
    address_country: DataTypes.STRING,
    phoneNumber: DataTypes.STRING,
    faxNumber: DataTypes.STRING,
    email: DataTypes.STRING,
    ein: DataTypes.STRING,
    businessType: DataTypes.STRING,
    ipnUrl: DataTypes.STRING,
    apiSecret: DataTypes.STRING,
    apiHash: DataTypes.STRING,
    status: {
      type: DataTypes.ENUM,
      values: ['active', 'inactive', 'deleted'],
      defaultValue: 'active'
    }
  }, {});

  Merchant.associate = function(models) {
    Merchant.belongsToMany(models.User, {
      through: models.MerchantUser,
      foreignKey: 'merchant_id'
    });
  };

  return Merchant;
};
