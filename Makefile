# Makefile

clean:
	echo "Clean application:"

build:
	echo "Building all containers:"
	docker-compose build

start:
	echo "Starting all containers:"
	docker-compose up -d

stop:
	echo "Stopping all containers:"
	docker-compose down -v

logs:
	docker-compose logs -f

console:
	docker exec -it php /bin/sh

list:
	docker ps

migration_clear:
	node_modules/.bin/sequelize db:migrate:undo:all

migration:
	node_modules/.bin/sequelize db:migrate

start-dev:
	npm run start:dev
