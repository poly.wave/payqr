Merchant

1) Offline Merchant with cashier
    1) Cashier creates transaction
    2) Cashier get QR-code(transaction id in it)
      2.1) If noone scans QR-code in 5 minutes transaction gets timed-out
    3) User scans QR-code(gets the transaction id)
    4) User changes transaction status to 'pending', adds himself to this transaction, 
    so noone else can join it.
    5) User changes transaction status to 'accepted'/'declined'
[POST] /merchant/:id/transactions {[products]}
[GET] /merchant/:id/transactions/:transaction_id

2) Offline Merchant without cashier
  1) User scans QR-code(with product id in it)
  2) User loads the product with metadata and inventory/merchant
  3) User sends request to create transaction with pacrticular product, inventory etc..
  4) Skyqr creates transaction with status 'pending', and user connected to this transaction
  5) User changes transaction status to 'accepted'/'declined'

3) Online Merchant
 similar to 1)



